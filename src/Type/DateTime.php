<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Type;

use DateTimeImmutable;
use DateTimeInterface;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;

class DateTime extends ScalarType
{
    /**
     * @param DateTimeInterface $value
     */
    public function serialize(mixed $value): string
    {
        if (!$value instanceof DateTimeInterface) {
            throw new InvariantViolation(sprintf('Value must be DateTimeInterface, got "%s"', Utils::printSafe($value)));
        }
        return $value->format(DateTimeInterface::RFC3339);
    }

    /**
     * @param string $value
     */
    public function parseValue(mixed $value): DateTimeImmutable
    {
        if (!is_string($value)) {
            throw new InvariantViolation(sprintf('Value must be string, got "%s"', Utils::printSafe($value)));
        }

        $parseResult = DateTimeImmutable::createFromFormat(DateTimeInterface::RFC3339, $value);

        if (!$parseResult) {
            throw new InvariantViolation('Value is not an RFC3339 formatted date');
        }

        return $parseResult;
    }

    /**
     * @param StringValueNode $valueNode
     *
     * @throws Error
     * @throws InvariantViolation
     */
    public function parseLiteral(mixed $valueNode, ?array $variables = null): DateTimeImmutable
    {
        if (!$valueNode instanceof Node) {
            throw new InvariantViolation(
                sprintf('Value must be "GraphQL\Language\AST\Node", got "%s"', Utils::printSafe($valueNode))
            );
        }
        if (!$valueNode instanceof StringValueNode) {
            throw new Error(
                sprintf('Can only parse strings, got "%s"', $valueNode->kind),
                [$valueNode]
            );
        }
        return $this->parseValue($valueNode->value);
    }
}
