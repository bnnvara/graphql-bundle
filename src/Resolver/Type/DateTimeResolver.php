<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Resolver\Type;

use BNNVARA\GraphQlBundle\Type\DateTime;

class DateTimeResolver
{
    public function __invoke(): DateTime
    {
        return new DateTime();
    }
}
