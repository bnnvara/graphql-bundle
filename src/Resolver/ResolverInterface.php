<?php
declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Resolver;

use ArrayObject;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\ArgumentInterface;

interface ResolverInterface
{
    public function __invoke(
        ?object $parentValue,
        ArgumentInterface $args,
        ArrayObject $context,
        ResolveInfo $info
    ): mixed;

}
