<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle;

use BNNVARA\GraphQlBundle\DependencyInjection\BNNVARAGraphQlExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class BNNVARAGraphQlBundle extends AbstractBundle
{
    // TODO : volgens de Symfony docs zou dit niet nodig hoeven zijn, maar ik krijg de tests niet gefixed zonder
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new BNNVARAGraphQlExtension();
    }
}
