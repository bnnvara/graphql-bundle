<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\EventListener;

use Overblog\GraphQLBundle\Event\ErrorFormattingEvent;
use Sentry\State\HubInterface;
use Throwable;

class SendExceptionToSentryListener
{
    public function __construct(
        private ?HubInterface $hub
    ) {
    }

    public function sendToSentry(ErrorFormattingEvent $event): void
    {
        if ($this->hub === null) {
            return;
        }

        $exception = $event->getError()?->getPrevious();

        if (!$exception instanceof Throwable) {
            $exception = $event->getError();
        }

        $this->hub->captureException($exception);
    }
}
