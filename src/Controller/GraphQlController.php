<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Controller;

use Overblog\GraphQLBundle\Request\Executor;
use Overblog\GraphQLBundle\Request\Parser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * This class is a simplified version of 'overblog/graphql-bundle/src/Controller/GraphController.php'
 * It exists to drop 'batch' functionality and to return a non-200 HTTP status code in case of error.
 */
class GraphQlController
{
    public function __construct(
        private Parser $requestParser,
        private Executor $requestExecutor
    ) {
    }

    public function __invoke(Request $request, string $schemaName = null): JsonResponse
    {
        if ($request->getMethod() === 'OPTIONS') {
            return new JsonResponse([], Response::HTTP_OK);
        }

        if ($request->getMethod() !== 'POST') {
            return new JsonResponse('', Response::HTTP_METHOD_NOT_ALLOWED);
        }

        $params = $this->requestParser->parse($request);

        $payload = $this->requestExecutor->execute($schemaName, $params)->toArray();

        $httpStatus = Response::HTTP_OK;
        if (isset($payload['errors'])) {
            $httpStatus = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($payload, $httpStatus);
    }
}
