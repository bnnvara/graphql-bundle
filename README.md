# README

##Installation

Using Composer:
```
composer require bnnvara/graphql-bundle
```

Enable the bundle in `bundles.php`.
```
return [
    ...
    BNNVARA\GraphQLBundle\BNNVARAGraphQlBundle::class => ['all' => true],
    ...
];
```