# Changelog
All notable changes to this project will be documented in this file.
Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.0] - 2022-08-05
### Added
- support for all DateTimeInterface implementations in resolver

### Changed
- make Sentry hub optional
- renamed types and resolvers

## [3.0.1] - 2022-07-22
### Added
- send exception to Sentry

## [3.0] - 2022-07-22
### Added
- overblog/graphql-bundle support
- PHP 8.1 support
- Symfony 6.1 support
- config in YAML

### Changed
- .editorconfig

### Removed
- bnnvara/graphql support
- config in XML
- support for caching
- support for logger

## [2.0.1] - 2020-06-10
### Changed
- version updates for
    - php
    - graphql
