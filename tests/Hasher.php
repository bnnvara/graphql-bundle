<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests;

trait Hasher
{
    private function getHashFromString(string $string): string
    {
        return hash(
            'sha512',
            $string . 'saltbnnvarasalt'
        );
    }

    private function getHashFromFileContents(string $filename): string
    {
        return $this->getHashFromString(file_get_contents($filename));
    }
}
