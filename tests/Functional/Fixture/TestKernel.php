<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Functional\Fixture;

use BNNVARA\GraphQlBundle\BNNVARAGraphQlBundle;
use Overblog\GraphQLBundle\OverblogGraphQLBundle;
use Sentry\SentryBundle\SentryBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel;

class TestKernel extends Kernel
{
    use MicroKernelTrait;

    private ?string $cacheDir = null;

    public function getProjectDir(): string
    {
        return __DIR__;
    }

    public function getCacheDir(): string
    {
        if ($this->cacheDir === null) {
            $this->cacheDir = implode(
                DIRECTORY_SEPARATOR,
                [sys_get_temp_dir(), str_replace("\\", ".", __CLASS__)]
            );
            $wipe = function (string $dir) use (&$wipe): void {
                foreach (scandir($dir) as $child) {
                    $path = $dir . DIRECTORY_SEPARATOR . $child;
                    if ("" === trim($child, ".")) {
                        continue;
                    }
                    if (is_dir($path)) {
                        $wipe($path);
                        continue;
                    }
                    unlink($path);
                }
            };
            if (is_dir($this->cacheDir)) {
                $wipe($this->cacheDir);
            }
        }
        return $this->cacheDir;
    }

    public function registerBundles(): iterable
    {
        yield new OverblogGraphQLBundle();
        yield new SentryBundle();
        yield new BNNVARAGraphQlBundle();
        yield new FrameworkBundle();
    }
}
