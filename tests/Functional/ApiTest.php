<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Functional;

use BNNVARA\GraphQlBundle\Tests\Functional\Fixture\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    public static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    /**
     * @test
     */
    public function canDispatchAndCatchException(): void
    {
        $params = [
            'query' => 'test',
        ];

        $client = static::createClient();

        $client->request(
            method: 'POST',
            uri: '/api/somethingsomething/graphql/',
            server: [
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json',
                'REMOTE_ADDR' => '127.0.0.1',
                'HTTP_CLIENT_IP' => '127.0.0.1',
                'HTTP_X_FORWARDED_FOR' => '127.0.0.1',
            ],
            content: json_encode($params)
        );

        $response = $client->getResponse();

        $content = json_decode($response->getContent(), true);

        $this->assertSame(500, $response->getStatusCode());
        $this->assertSame('At least one schema should be declare.', $content['detail']);
    }
}
