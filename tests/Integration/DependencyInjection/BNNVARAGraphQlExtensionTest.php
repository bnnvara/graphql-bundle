<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Integration\DependencyInjection;

use BNNVARA\GraphQlBundle\DependencyInjection\BNNVARAGraphQlExtension;
use BNNVARA\GraphQlBundle\Resolver\Type\DateTimeResolver;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;

class BNNVARAGraphQlExtensionTest extends AbstractExtensionTestCase
{
    /** @test */
    public function servicesConfigurationIsLoaded(): void
    {
        $this->load();

        $this->assertContainerBuilderHasService('bnnvara_graphql.controller');
        $this->assertContainerBuilderHasService('bnnvara_graphql.resolver.type.datetime');
        $this->assertContainerBuilderHasService(DateTimeResolver::class);
        $this->assertContainerBuilderHasService('bnnvara_graphql.event_listener.send_exception_to_sentry');
    }

    protected function getContainerExtensions(): array
    {
        return [
            new BNNVARAGraphQlExtension(),
        ];
    }
}
