<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Unit\Controller;

use BNNVARA\GraphQlBundle\Controller\GraphQlController;
use GraphQL\Executor\ExecutionResult;
use Overblog\GraphQLBundle\Request\Executor;
use Overblog\GraphQLBundle\Request\Parser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GraphQlControllerTest extends TestCase
{
    /**
     * @test
     */
    public function optionsMethodOnEndpointActionReturns200(): void
    {
        $request = Request::create('/graphql', 'OPTIONS');

        $parser = $this->createMock(Parser::class);
        $executor = $this->createMock(Executor::class);

        $controller = new GraphQlController(
            $parser,
            $executor
        );

        $response = $controller($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('[]', $response->getContent());
    }

    /**
     * @test
     */
    public function unsupportedMethodOnEndpointActionReturns405(): void
    {
        $request = Request::create('/graphql', 'GET');

        $parser = $this->createMock(Parser::class);
        $executor = $this->createMock(Executor::class);

        $controller = new GraphQlController(
            $parser,
            $executor
        );

        $response = $controller($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(405, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function postMethodOnEndpointActionReturns200WithCorrectPayload(): void
    {
        $request = Request::create('/graphql', 'POST');

        $parser = $this->createMock(Parser::class);
        $parser->expects($this->once())->method('parse')
            ->with($request)
            ->willReturn(['param1' => 'value1']);

        $executionResult = $this->createMock(ExecutionResult::class);
        $executionResult->method('toArray')->willReturn(['result' => 'test result']);

        $executor = $this->createMock(Executor::class);
        $executor->expects($this->once())->method('execute')
            ->with(null, ['param1' => 'value1'])
            ->willReturn($executionResult);

        $controller = new GraphQlController(
            $parser,
            $executor
        );

        $response = $controller($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('{"result":"test result"}', $response->getContent());
    }

    /**
     * @test
     */
    public function postMethodOnEndpointActionReturns500OnError(): void
    {
        $request = Request::create('/graphql', 'POST');

        $parser = $this->createMock(Parser::class);
        $parser->expects($this->once())->method('parse')
            ->with($request)
            ->willReturn(['param1' => 'value1']);

        $executionResult = $this->createMock(ExecutionResult::class);
        $executionResult->method('toArray')->willReturn(
            [
                'result' => 'test result',
                'errors' => [
                    'big error',
                ],
            ]
        );

        $executor = $this->createMock(Executor::class);
        $executor->expects($this->once())->method('execute')
            ->with(null, ['param1' => 'value1'])
            ->willReturn($executionResult);

        $controller = new GraphQlController(
            $parser,
            $executor
        );

        $response = $controller($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertSame(500, $response->getStatusCode());
        $this->assertSame('{"result":"test result","errors":["big error"]}', $response->getContent());
    }
}
