<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Unit\EventListener;

use BNNVARA\GraphQlBundle\EventListener\SendExceptionToSentryListener;
use Exception;
use GraphQL\Error\Error;
use Overblog\GraphQLBundle\Event\ErrorFormattingEvent;
use PHPUnit\Framework\TestCase;
use Sentry\State\HubInterface;

class SendExceptionToSentryListenerTest extends TestCase
{
    /**
     * @test
     */
    public function nothingHappensWhenHubIsNull(): void
    {
        $errorEvent = $this->createMock(ErrorFormattingEvent::class);
        $errorEvent->expects($this->never())->method('getError');

        $hub = null;

        $listener = new SendExceptionToSentryListener($hub);

        $listener->sendToSentry($errorEvent);
    }

    /**
     * @test
     */
    public function previousExceptionIsSentToSentryIfItIsThrowable(): void
    {
        $exception = new Exception('test exception');

        $error = new Error(
            message: 'test error',
            previous: $exception
        );

        $errorEvent = $this->createMock(ErrorFormattingEvent::class);
        $errorEvent->method('getError')->willReturn($error);

        $hub = $this->createMock(HubInterface::class);
        $hub->expects($this->once())->method('captureException')->with($exception);

        $listener = new SendExceptionToSentryListener($hub);

        $listener->sendToSentry($errorEvent);
    }

    /**
     * @test
     */
    public function sendCurrentErrorToSentryIfThereIsNoPreviousErrorThatIsThrowable(): void
    {
        $error = new Error(
            message: 'test error',
        );

        $errorEvent = $this->createMock(ErrorFormattingEvent::class);
        $errorEvent->method('getError')->willReturn($error);

        $hub = $this->createMock(HubInterface::class);
        $hub->expects($this->once())->method('captureException')->with($error);

        $listener = new SendExceptionToSentryListener($hub);

        $listener->sendToSentry($errorEvent);
    }
}
