<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Unit\Type;

use BNNVARA\GraphQlBundle\Type\DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{
    /** @test */
    public function dateTimeTypeExtendsGraphQlScalarType(): void
    {
        $this->assertTrue(is_subclass_of(DateTime::class, ScalarType::class));
    }

    /**
     * @test
     * @dataProvider dateTimeProvider
     */
    public function dateTimeObjectIsSerializedToDateString(DateTimeInterface $dateTime, string $expectedString): void
    {
        $dateTimeType = new DateTime();
        $actual = $dateTimeType->serialize($dateTime);
        $this->assertSame($expectedString, $actual);
    }

    /** @test */
    public function serializeThrowsExceptionWhenInvalidArgument(): void
    {
        $dateTimeType = new DateTime();
        $this->expectException(InvariantViolation::class);
        $dateTimeType->serialize('datetime');
    }

    /** @test */
    public function dateStringIsSerializedToImmutableDateTimeObject(): void
    {
        $dateTimeType = new DateTime();
        $actual = $dateTimeType->parseValue('2018-06-15T22:07:02+00:00');
        $this->assertEquals(new DateTimeImmutable('2018-06-15T22:07:02+00:00'), $actual);
    }

    /** @test */
    public function parseValueThrowsExceptionWhenArgumentNotString(): void
    {
        $dateTimeType = new DateTime();
        $this->expectException(InvariantViolation::class);
        $dateTimeType->parseValue((object) []);
    }

    /** @test */
    public function parseValueThrowsExceptionWhenArgumentNotRFC3339String(): void
    {
        $dateTimeType = new DateTime();
        $this->expectException(InvariantViolation::class);
        $dateTimeType->parseValue('2018-06-15 22:07:02+00:00');
    }

    /** @test */
    public function canParseStringNodeToGetDateTimeImmutableObject(): void
    {
        $dateTimeType = new DateTime();
        $actual = $dateTimeType->parseLiteral(new StringValueNode(['value' => '2018-06-15T22:07:02+00:00']));
        $this->assertEquals(new DateTimeImmutable('2018-06-15T22:07:02+00:00'), $actual);
    }

    /** @test */
    public function parseLiteralThrowsExceptionWhenArgumentNotNode(): void
    {
        $dateTimeType = new DateTime();
        $this->expectException(InvariantViolation::class);

        $dateTimeType->parseLiteral((object) []);
    }

    /** @test */
    public function parseLiteralThrowsExceptionWhenArgumentNotStringNode(): void
    {
        $dateTimeType = new DateTime();
        $node = new class([]) extends Node {
            public string $kind = 'something';
        };

        $this->expectException(Error::class);

        $dateTimeType->parseLiteral($node);
    }

    public function dateTimeProvider(): array
    {
        return [
            'dateTimeImmutable' => [
                'dateTime' => new DateTimeImmutable('2018-06-15T22:07:02+00:00'),
                'expectedString' => '2018-06-15T22:07:02+00:00',
            ],
            'dateTime' => [
                'dateTime' => new DateTimeImmutable('2019-03-17T21:08:00+00:00'),
                'expectedString' => '2019-03-17T21:08:00+00:00',
            ],
        ];
    }
}
