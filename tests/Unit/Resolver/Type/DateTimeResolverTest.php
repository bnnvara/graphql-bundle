<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Unit\Resolver\Type;

use BNNVARA\GraphQlBundle\Resolver\Type\DateTimeResolver;
use BNNVARA\GraphQlBundle\Type\DateTime;
use PHPUnit\Framework\TestCase;

class DateTimeResolverTest extends TestCase
{
    /** @test */
    public function invokeReturnsInstanceOfDateTimeType(): void
    {
        $this->assertInstanceOf(DateTime::class, (new DateTimeResolver())());
    }
}
