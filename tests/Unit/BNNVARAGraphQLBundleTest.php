<?php

declare(strict_types=1);

namespace BNNVARA\GraphQlBundle\Tests\Unit;

use BNNVARA\GraphQlBundle\BNNVARAGraphQlBundle;
use BNNVARA\GraphQlBundle\DependencyInjection\BNNVARAGraphQlExtension;
use PHPUnit\Framework\TestCase;

class BNNVARAGraphQLBundleTest extends TestCase
{
    /** @test */
    public function bundleLoadsExtension(): void
    {
        $bundle = new BNNVARAGraphQlBundle();

        $extension = $bundle->getContainerExtension();

        $this->assertInstanceOf(BNNVARAGraphQlExtension::class, $extension);
    }
}
